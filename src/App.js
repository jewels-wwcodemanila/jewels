import React, { Component } from 'react';
import Header from './Header';
import Category from './Category';
import Quote from './Quote';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      category: ""
    }
    this.onCategorySelected = this.onCategorySelected.bind(this)
  }

  onCategorySelected(category) {
    this.setState({
      category
    })
  }

  render() {
    return (
      <div className="App">
        <Header />
        <div className="Content">
          <Category categorySelect={(e) => this.onCategorySelected(e)}/>
          <Quote category={this.state.category} />
        </div>
      </div>
    );
  }
}

export default App;
