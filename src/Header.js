import React, { Component } from 'react';
import jewel from './jewel.gif';
import './App.css';


class Header extends Component {
  render() {
    return (

        <header className="Main-header">

          <img src={jewel} className="App-logo" alt="logo" /><br></br><br></br><br></br><br></br><br></br>
          <h1 className="App-title">JEWELS</h1>
        </header>

    );
  }
}

export default Header;
