import React, { Component } from 'react';
import './Quote.css';


class Quote extends Component {
    render() {
      if (this.props.category === "friendship") {
        return (
            <div className="quote">
            	<div className="row">
        			  <div className="grid-item bg1">
                  Friendship is the hardest thing in the world to explain. Its not something you learn in school. But if you havent learned the meaning of friendship, you really havent learned anything.
                  <br />
                  -Muhammad Ali
        			  </div>
        			  <div className="grid-item bg2">
                  Friendship is born at that moment when one person says to another: What! You too? I thought I was the only one.
                  <br />
        			  </div>
        			  <div className="grid-item bg3">
                  Nothing makes the earth seem so spacious as to have friends at a distance; they make the latitudes and longitudes
                  <br />
                </div>
        		 	 </div>
        		 	 <div className="row">
          			  <div className="grid-item bg3">
          			  	Few there are that will endure a true friend.
                    <br />

          			  </div>
          			  <div className="grid-item bg1">
                    I would rather walk with a friend in the dark, than alone in the light.
                    <br />

          			  </div>
          			  <div className="grid-item bg2">
                    No friendship is an accident.
                    <br />

                  </div>
        		 	</div>
        		 	<div className="row">
        			  <div className="grid-item bg2">
        			  	A true friend is someone who is there for you when hed rather be anywhere else.
                  <br />

        			  </div>
        			  <div className="grid-item bg3">
                  You kind of need to weed through who is wanting to be a true friend.
                  <br />

        			  </div>
        			  <div className="grid-item bg1">
                  I dont know if Ive ever been in a clique. The older Ive gotten, the more Ive realized what a true friend really is. So my friendship circle has changed a bit.
                  <br />

                </div>
        		 	</div>
    		</div>
        );
      }
      if (this.props.category === "love") {
        return (
            <div className="quote">
              <div className="row">
                <div className="grid-item bg1">
                  <div className="text">

                  A loving heart is the truest wisdom.
                  <br />
                  </div>

                </div>
                <div className="grid-item bg2">
                  Love begins with a smile, grows with a kiss, and ends with a teardrop.
                  <br />
                </div>
                <div className="grid-item bg3">
                  Look beyond the naked eye and you will see the beauty of ones soul, and the true essence of who they really are.
                  <br />

                </div>
               </div>
               <div className="row">
                  <div className="grid-item bg3">
                    The spaces between your fingers were created so that anothers could fill them in.
                    <br />

                  </div>
                  <div className="grid-item bg1">
                    A loving heart is the truest wisdom.
                    <br />

                  </div>
                  <div className="grid-item bg2">
                    No matter how badly your heart is broken, the world does not stop for your grief.
                    <br />

                  </div>
              </div>
              <div className="row">
                <div className="grid-item bg2">
                  Can there be a love which does not make demands on its object?
                  <br />

                </div>
                <div className="grid-item bg3">
                  Kung ang isang buong araw ay binubuo ng 86,400 segundo. Anong pake ko? Kung ikaw lang naman ang kailangan para mabuo ang araw ko!
                  <br />

                </div>
                <div className="grid-item bg1">
                  Love begins with a smile, grows with a kiss, and ends with a teardrop.
                  <br />

                </div>
              </div>
        </div>
        );
      }

      return (
          <div className="quote">
            <div className="row">
              <div className="grid-item bg1">
                Failure will never overtake me if my determination to succeed is strong enough.
                <br/ >

              </div>
              <div className="grid-item bg2">
                Success is not final, failure is not fatal: it is the courage to continue that counts.
                <br/ >

              </div>
              <div className="grid-item bg3">
                There are no secrets to success. It is the result of preparation, hard work, and learning from failure.
                <br/ >

              </div>
             </div>
             <div className="row">
                <div className="grid-item bg3">
                  Life is 10% what happens to you and 90% how you react to it.
                  <br/ >

                </div>
                <div className="grid-item bg1">
                  Good, better, best. Never let it rest. Til your good is better and your better is best.
                  <br/ >

                </div>
                <div className="grid-item bg2">
                  Only I can change my life. No one can do it for me.
                  <br/ >

                </div>
            </div>
            <div className="row">
              <div className="grid-item bg2">
                It does not matter how slowly you go as long as you do not stop.
                <br/ >

              </div>
              <div className="grid-item bg3">
                Nothing in this world can take the place of persistence. Talent will not: nothing is more common than unsuccessful men with talent. Genius will not; unrewarded genius is almost a proverb. Education will not: the world is full of educated derelicts. Persistence and determination alone are omnipotent.
                <br/ >

              </div>
              <div className="grid-item bg1">
                With the new day comes new strength and new thoughts.
                <br/ >

              </div>
            </div>
      </div>
      );
  }
}

export default Quote;
