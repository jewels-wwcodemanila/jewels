import React, { Component } from 'react';
import './Category.css';

class Category extends Component {
  constructor(props) {
    super(props);
    this.state = {
      category: ''
    };
    this.CategoryLifeClick = this.CategoryLifeClick.bind(this);
    this.CategoryLoveClick = this.CategoryLoveClick.bind(this);
    this.CategoryFriendshipClick = this.CategoryFriendshipClick.bind(this);
  }

  CategoryLoveClick() {
    this.props.categorySelect("love");
  }

  CategoryLifeClick() {
    this.props.categorySelect("life");
  }

  CategoryFriendshipClick() {
    this.props.categorySelect("friendship");
  }

  render() {
    return (
        <div className="category">
          <div className="vertical-menu">
            <h1><font color="white"> Category</font></h1>
            <a onClick={() => this.CategoryLoveClick()}>Love Quotes</a>
            <a onClick={() => this.CategoryFriendshipClick()}>Friendship Quotes</a>
            <a onClick={() => this.CategoryLifeClick()}>Inspirational Quotes</a>
          </div>
        </div>
    );
  }
}

export default Category;
